package mx.com.elektra.testerpresupuestacion

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import dagger.hilt.android.AndroidEntryPoint
import mx.com.elektra.presupuestacion.ui.actvities.MainActivity
import mx.com.elektra.presupuestacion.utils.DataHolder
import mx.com.elektra.presupuestacion.utils.General
import mx.com.elektra.testerpresupuestacion.databinding.ActivityInitBinding

@AndroidEntryPoint
class InitActivity : AppCompatActivity() {
    var binding: ActivityInitBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.binding = ActivityInitBinding.inflate(layoutInflater)
        setContentView(this.binding!!.root)

        General.initPresupuestacion(
            numeroEmpleado = "117",
            idSucursal = 2784,
            radioSucursal = 100.0
        )

        this.binding!!.btnGoToProductos.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    mx.com.elektra.presupuestacion.ui.actvities.MainActivity::class.java
                )
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        this.binding = null
    }
}