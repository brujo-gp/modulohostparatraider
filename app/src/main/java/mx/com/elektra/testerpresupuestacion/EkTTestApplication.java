package mx.com.elektra.testerpresupuestacion;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;


@HiltAndroidApp
public class EkTTestApplication extends Application {
}
